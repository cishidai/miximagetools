'''
创建者：张宁
邮箱： 1342403905@qq.com
创建时间：2022年5月7日
程序功能： 实现两张图片的融合
'''
# coding=utf-8
import cv2
import os
import json
import argparse

def parse():
    parser = argparse.ArgumentParser()
    parser.add_argument('--json_path', type=str, dest="json_path", help="boxes json path")
    parser.add_argument('--mode_type',  type=str, dest='mode_type', help="mode is a or b")
    parser.add_argument('--ori_img_path', type=str, dest='ori_img_path', help="ori image path")
    parser.add_argument('--resize_img_path', type=str, dest='resize_img_path', help="resize image path")
    parser.add_argument('--save_img_path', type=str, dest='save_img_path', default=".", help="save image path")
    parser.add_argument('--save_img_name', type=str, dest='save_img_name', default="new.jpg", help="save image name")
    return parser.parse_args()

def parse_json(json_path, mode):
    str = ''
    with open(json_path, 'r', encoding="UTF-8") as file:
        for line in file.readlines():
            str += line
    dict_str = json.loads(str)
    x1, y1, x2, y2 = -1, -1, -1, -1
    for box_text in dict_str["boxes"]:
        if mode == 'a':
            if box_text["name"] == "box_a":
                x1, y1 = box_text["rectangle"]["left_top"]
                x2, y2 = box_text["rectangle"]["right_bottom"]
                break
        if mode == 'b':
            if box_text["name"] == "box_b":
                x1, y1 = box_text["rectangle"]["left_top"]
                x2, y2 = box_text["rectangle"]["right_bottom"]
                print("box_b rectangle left_top:{}, {},right_bottom:{}, {}".format(x1, y1, x2, y2))
                break

    return [x1, y1, x2, y2]

# 根据不同模式缩放图片
# a模式指拉伸填充
# b模式指保持原比例填充
def resize_img(resize_img_path, bbox, mode):
    re_img = cv2.imread(resize_img_path)
    scale_w = bbox[2] - bbox[0] + 1
    scale_h = bbox[3] - bbox[1] + 1
    if mode == 'a':
        re_img = cv2.resize(re_img, (scale_w, scale_h))
    elif mode == 'b':
        w_ = float(re_img.shape[1] / scale_w)
        h_ = float(re_img.shape[0] / scale_h)
        scale_ = max(w_, h_)
        re_img = cv2.resize(re_img, (int(re_img.shape[1]/scale_), int(re_img.shape[0]/scale_)))
    return re_img

# 将patch_img融合到ori_img
# 返回融合完成的ori_img
def mix_img(ori_img_path, patch_img, bbox):
    w = patch_img.shape[1]
    h = patch_img.shape[0]
    ori_img = cv2.imread(ori_img_path)
    #判断resize坐标是否超出原图坐标
    if bbox[2] > ori_img.shape[1] or bbox[3] > ori_img.shape[0]:
        return None
    for w_index in range(ori_img.shape[1]):
        for h_index in range(ori_img.shape[0]):
            if bbox[1] <= h_index and h_index < bbox[1] + h  and bbox[0] <= w_index and w_index < bbox[0] + w:
                ori_img[h_index, w_index, :] = patch_img[h_index - bbox[1], w_index - bbox[0], :]
    return ori_img

if __name__ =="__main__":
    args = parse()
    suffix_list = ['.png', '.PNG', '.JPG', '.jpg', '.jpeg', '.JPEG']
    if os.path.splitext(args.resize_img_path)[1] in suffix_list and \
       os.path.splitext(args.ori_img_path)[1] in suffix_list:
        bbox = parse_json(args.json_path, args.mode_type)
        if sum(bbox) == -4:
            print("Sorry, bbox data is wrong!")
        else:
            patch_img = resize_img(args.resize_img_path, bbox, args.mode_type)
            result_img = mix_img(args.ori_img_path, patch_img, bbox)
            if result_img is not None:
                cv2.imwrite(os.path.join(args.save_img_path, args.save_img_name), result_img)
            else:
                print("Please check your input bbox coordinates!")
            # cv2.imshow("result_img", result_img)
            # cv2.waitKey(0)
