# MixImageTools

实现两个图片的融合

## Installation
系统使用环境：windows10/ubuntu20.04

软件使用环境：python3.6+

安装依赖：
```
$pip install -r requirement.txt
```

```
$git clone https://gitlab.com/cishidai/miximagetools.git
$cd miximagetools
```

## Usage
```
$python3 main.py --json_path=./boxes.json --ori_img_path=1.jpg --resize_img_path=2.jpg --mode_type a --save_img_name=newa.jpg
```

参数说明：
```
    -h help帮助信息
    --json_path boxes.json的文件路径
    --mode_type 模式选择，可选参数a表示可拉伸填充模式，b表示保持原比例填充模式
    --ori_img_path 图片A路径，图片A是指用于被粘贴的图片
    --resize_img_path 图片B路径，图片B是指需要缩放，且粘贴到图片A的
    --save_img_path 图片A和图片B融合后的图片保存路径,默认是当前路径
    --save_img_name 图片A和图片B融合后的图片名称,默认是new.jpg

```
## Contributer

author:zhangning
